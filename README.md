# Engineer School Starter Pack

Ceci est un tutoriel destiné aux élèves en école d'ingénieur, et leur présente les principaux outils pour développer leurs projets informatique.

## Hébergement

Le site est disponible [à cette adresse](https://nicolasbfr.gitlab.io/engineer-school-starter-pack), et est hébergé par Gitlab Pages.

## Sujets abordés

- Outils de base
	- IDE Jetbrains
	- Github Copilot
	- Git
	- Gitlab
- Outils avancés
	- WSL2
	- Clés SSH sur Git
	- Docker
- Longs projets
	- Branches sur Git
	- Création de tests
		- Python
		- Java
		- C++
	- Exécution des tests sur Gitlab

## Licence

Le projet est sous [Licence MIT](LICENSE).