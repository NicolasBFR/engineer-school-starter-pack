# Outils avancés

## WSL2

Comme le dit la description de Microsoft:

> Les développeurs peuvent accéder simultanément à la puissance de Windows et de Linux sur une machine Windows. Le Sous-système Windows pour Linux (WSL) permet aux développeurs d’installer une distribution Linux (comme Ubuntu, OpenSUSE, Kali, Debian, Arch Linux, etc.) et d’utiliser des applications, des utilitaires et des outils en ligne de commande Bash Linux directement sur Windows, sans modification, sans devoir passer par une machine virtuelle traditionnelle ou une configuration à double démarrage.

Voici [le tutoriel pour l'installer](https://learn.microsoft.com/fr-fr/windows/wsl/install).

Une fois WSL installé, il vaut mieux désinstaller Git pour Windows et utiliser Git sur WSL [comme décrit ici](https://learn.microsoft.com/fr-fr/windows/wsl/tutorials/wsl-git). 

## Clés SSH sur Git

Vous aviez peut-être pris l'habitude de rentrer vos identifiants Gitlab à chaque modification du dépôt Gitlab, mais la véritable manière de s'authentifier est la clé SSH. Voici le [tutoriel Gitlab](https://docs.gitlab.com/ee/user/ssh.html) pour utiliser les clés SSH sur Gitlab, et [un tutoriel spécifique](https://ardalis.com/generate-ssh-rsa-keys-windows-wsl/) pour créer ces clés sous WSL.

## Docker

> Docker est une plate-forme logicielle qui vous permet de concevoir, tester et déployer des applications rapidement. Docker intègre les logiciels dans des unités normalisées appelées conteneurs, qui rassemblent tous les éléments nécessaires à leur fonctionnement, dont les bibliothèques, les outils système, le code et l'environnement d'exécution. Avec Docker, vous pouvez facilement déployer et dimensionner des applications dans n'importe quel environnement, avec l'assurance que votre code s'exécutera correctement.
>
> [Source: AWS](https://aws.amazon.com/fr/docker/)

Docker peut-être installé sur WSL, afin d'utiliser des logiciels difficiles à installer, ou non disponibles sur Windows, comme MySQL ou autres.

Un autre intérêt est de créer des [images Docker](https://jfrog.com/knowledge-base/a-beginners-guide-to-understanding-and-building-docker-images/) à partir de vos projets ou sous-projets, afin de permettre à des collègues d'utiliser votre projet pour le leur (Par exemple un [frontend et un backend](https://www.wildcodeschool.com/fr-FR/blog/differences-backend-frontend-developpement-web)). La pipeline de Gitlab peut créer des images Docker à partir des projets [comme décrit ici](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html).
