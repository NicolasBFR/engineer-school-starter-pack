# Pour les longs projets

Si votre projet dure plus de quatre ou cinq semaines, il vaudrait mieux appliquer les techniques ci-dessous.

## Branches sur Git

D'après la documentation de Git "`Créer une branche signifie diverger de la ligne principale 
de développement et continuer à travailler sans impacter cette ligne.`""
Les branches permettent de travailler indépendament de son côté sans avoir de problèmes de conflits avec d'autres personnes, ou des problèmes avec
d'autres fonctionnalités en cours de développement.

[Ce tutoriel officiel](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Les-branches-en-bref) explique très bien comment s'en servir.

## Création de tests

Il est important d'écrire des tests afin que l'on puisse régulièrement vérifier que des modifications n'aient pas cassé de fonctions du programme.
Parfois lors du développement, un changement peut avoir des conséquences très difficiles à prévoir.

Il existe [deux types de tests](https://mobiskill.fr/blog/conseils-emploi-tech/tests-unitaires-vs-tests-fonctionnels-quelles-differences/), les tests unitaires, qui vérifient une ou quelques fonctions, et les tests fonctionnels, qui exécute votre programme comme un utilisateur. Nous allons nous concentrer sur les tests unitaires mais Gitlab dispose [de ressources](https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html#integration-tests) sur les tests fonctionnels. 

Voici les principaux frameworks de tests unitaires pour chaque langage.

### Python

[Unittest](https://docs.python.org/fr/3/library/unittest.html) est inclu par défaut dans Python et intégré dans PyCharm, avec [un tutoriel d'utilisation](https://www.jetbrains.com/help/pycharm/testing-your-first-python-application.html).

### Java

[Junit](https://junit.org/junit5/docs/current/user-guide/) est considéré comme le framework par défaut pour les tests, et intégré dans Intellij Idea avec [une documentation complète](https://www.jetbrains.com/help/idea/tests-in-ide.html).

### C++

- [GTest](https://google.github.io/googletest/) est un autre framework de test assez bien fait et [intégré dans CLion](https://www.jetbrains.com/help/clion/creating-google-test-run-debug-configuration-for-test.html).

- Eventuellement [Ctest](https://cmake.org/cmake/help/latest/manual/ctest.1.html), car il s'intègre rapidement si vous construisez votre projet avec CMake. Voici [un tutoriel](https://cmake.org/cmake/help/book/mastering-cmake/chapter/Testing%20With%20CMake%20and%20CTest.html).


### Autres langages

Il existe beaucoup d'autres frameworks de tests [pour tous les langages](https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks).

## Exécution des tests sur Gitlab

Il y a un [excellent tutoriel en français](https://blog.stephane-robert.info/post/introduction-gitlab-ci/) expliquant très bien comment faire des pipelines, ainsi que le [tutoriel officiel](https://docs.gitlab.com/ee/ci/quick_start/).

Optionnellement, pour éviter d'avoir à lire les logs de pipelines, vous pouvez publier leurs résultats avec le [Unit test reports](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) (C'est optionnel mais augmente la productivité à long terme).

## Techniques avancées

Ces techniques sont destinées à des projets de plusieurs mois, impliquant de nombreuses personnes ou encore destinés à être repris par d'autres personnes.

### Couverture de code

La [couverture de code](https://fr.wikipedia.org/wiki/Couverture_de_code) permet de vérifier la proportion de code vérifiée par un test. Gitlab a une fonctionalité pour trouver les lignes de code non couvertes par des tests [expliquée ici](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html+).

### Génération de documentation

Vous pouvez déployer des sites statiques (uniquement faits d'HTML/CSS/JS) sur [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/). Voici [un exemple pour générer une Javadoc](https://gitlab.com/java-devops-experiments/javadoc-from-project) à partir d'un projet. 