# Les outils pour survoler l'informatique

Les cours d'informatiques en école d'ingé sont souvent mauvais, faits avec et sur des outils datés, et recyclés sur 10 ans.
Le but de ce site est d'aider les étudiants à travailler sur les meilleurs outils pour booster leur productivité et éviter le
chaos lors de projets de groupe. Cela va d'abord demander beaucoup de création de compte et être pénible, mais ça en vaut la peine !

La première chose à faire est de se [créer un compte Github](https://github.com/signup) et [d'immédiatement demander l'offre étudiante](https://education.github.com/discount_requests/application) (ce qui prend au maximum quelques jours, mais est généralement instantané) en envoyant son certificat de scolarité ([converti de pdf en jpg](https://www.ilovepdf.com/fr/pdf_en_jpg)), puis de se mettre un rappel chaque année jusqu'au diplôme.

Vous aurez ensuite accès au [Github Student Pack](https://education.github.com/pack), qui est bourré d'offres: noms de domaines, hébergements, cours gratuits... 

## Coder rapidement
Il est important d'avoir les bons outils pour développer.

### Editeurs de code Jetbrains

Une fois approuvé par Github pour son offre étudiante, allez sur la page du [Github Student Pack](https://education.github.com/pack) et récupérez l'offre de Jetbrains. S'il vous demande de créer un compte, **connectez-vous avec Github**. Vous aurez ensuite [tous les produits Jetbrains](https://www.jetbrains.com/all/). Installez les éditeurs de code qu'il vous faut (CLion pour le C, Pycharm pour Python, Intellij Idea pour Java...).

### Github Copilot

*GitHub Copilot* est une IA qui permet d'autocompléter ce que vous écrivez, et qui peut être intégré dans vos éditeurs Jetbrains. Prenez-le [à cette page](https://github.com/features/copilot) (en cliquant sur l'essai gratuit, qui se transformera en abonnement grâce à votre offre étudiante).

Mettez ensuite le plugin Copilot dans votre éditeur comme expliqué [ici](https://docs.github.com/fr/enterprise-cloud@latest/copilot/getting-started-with-github-copilot) ou [ici](https://docs.github.com/en/copilot/getting-started-with-github-copilot?tool=jetbrains#installing-the-github-copilot-extension-in-your-jetbrains-ide).

Copilot ne remplace néanmoins pas un cerveau, ses suggestions ne feront pas forcément ce que vous voulez, voir ne marcheront pas. Il est de plus assez limité quand il s'agit d'avoir une vue d'ensemble d'un projet large.

### Bien déboguer

> Le débogage est un processus de diagnostic, de localisation et d’élimination des erreurs des programmes informatiques; ce processus permet aussi la vérification (autrement dit le test) du programme en cours d’élaboration
>
> Linguisticae investigationes, Volume 22, 1998

Le débogage consiste à utiliser [un débogueur](https://fr.wikipedia.org/wiki/D%C3%A9bogueur) pour faire superviser l'exécution du programme étape par étape, ou à certaines étape, en ayant les valeurs de toutes les variables, la mémoire, et toutes les informations nécessaires pour traquer un bug ou un comportement inatendu de votre programme.

Les programmes vous donnent généralement la ligne ou le programme a crashé, mais pas les variables d'entrée dans la fonction ni la raison pour laquelle elle a été appelée avec de mauvais arguments, et ne vous donnera aucune information lors mauvais comportements sans crashs.

Heureusement les éditeurs Jetbrains disposent de débogueurs professionnels (qui se ressemblent entre éditeurs). La documentation Pycharm dispose d'[un excellent tutoriel](https://www.jetbrains.com/help/pycharm/part-1-debugging-python-code.html) pour apprendre le débogage à partir de zéro, et Jetbrains fournit également de la documentation par éditeur:

- PyCharm (pour Python)
	- [Le tutoriel PyCharm](https://www.jetbrains.com/help/pycharm/part-1-debugging-python-code.html) cité au dessus. 
	- [La documentation PyCharm](https://www.jetbrains.com/help/pycharm/debugging-code.html) des sections `Breakpoints` à `Step through the program` inclue.

- Intellij Idea (pour Java)
	- [Le tutoriel Intellij](https://www.jetbrains.com/help/idea/debugging-your-first-java-application.html) ainsi que celui [pour changer des valeurs pendant l'exécution](https://www.jetbrains.com/help/idea/tutorial-set-value.html).
	- [La documentation Intellij Idea](https://www.jetbrains.com/help/idea/debugging-code.html) des sections `Breakpoints` à `Step through the program` inclue.

- CLion (pour C/C++)
	- [La documentation CLion](https://www.jetbrains.com/help/pycharm/debugging-code.html) la section `Start/pause/stop debug session` puis les sections `Examine suspended program` à `Watchpoints` inclue.
	- Si les [fuites de mémoire](https://fr.wikipedia.org/wiki/Fuite_de_m%C3%A9moire) sont pénalisées, utilisez [Google sanitizers](https://www.jetbrains.com/help/clion/google-sanitizers.html), ou encore [Valgrind](https://www.jetbrains.com/help/clion/memory-profiling-with-valgrind.html) (plus standard et plus puissant, mais nécessite [d'installer WSL2](advanced.md))

## Faire du code collaboratif
La partie précédente booste votre productivité individuelle, mais ne suffira pas pour des projets de groupe.
Il va vous falloir apprendre les méthodes de partage de code, en direct ou en différé.

### Code with me
*Code with me* est une sorte de partage d'écran intéractif entre plusieurs utilisateurs d'éditeurs Jetbrains (semblable à un Google Doc spécialisé pour du code),
qui permet d'éditer les fichiers de manière collaborative.

Son utilisation est très simple et expliquée [à cette page](https://www.jetbrains.com/help/idea/code-with-me.html#work_host).

### Git
Pour les projets de plus de quelques jours, il va vous falloir apprendre à *versionner*, et à se partager les versions du code. Git est le système de versionnage le plus commun. Il consiste à rapatrier en envoyer les modifications faites localement à un *hôte*, de manière à toujours avoir les notifications de tout le monde, tant en pouvant les annuler.

Voici des ressources:

- [Le tutoriel officiel](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-%C3%80-propos-de-la-gestion-de-version) est verbeux mais très clair et complet.

- [Un tuto du CNRS](https://perso.liris.cnrs.fr/pierre-antoine.champin/enseignement/intro-git/) très rapide à l'inverse.

- [Un tutoriel trop succint](https://blent.ai/blog/a/git-tutoriel-complet) mais qui aborde tout.

- [Un dernier](https://www.ionos.fr/digitalguide/sites-internet/developpement-web/tutoriel-git/) du même acabit.

### Gitlab

Il va vous falloir héberger vos dépôts Git. GitHub et GitLab sont des services d'hébergement de projets Git. Ils ont le rôle de serveur distant qui va héberger un projet Git. Il faut préférer Gitlab, car il est plus complet, standard dans le monde de l'entreprise.

- La Fabrique du numérique a fait [un excellent tutoriel](https://github.com/SocialGouv/tutoriel-gitlab#inscription) à ce sujet.

- [Les ressources Gitlab](https://docs.gitlab.com/ee/tutorials/) sont également complètes.
