# Héberger un projet

Il vous faudra parfois héberger un projet web PHP sur un hébergement web.

## Projet PHP

Pour un projet en PHP, prenez l'[hébergement offert](https://www.infomaniak.com/fr/support/faq/2229/infomaniak-student-web-mail-gratuit-pour-les-etudiantes) aux étudiants Infomaniak.

Ensuite, suivez [ce tutoriel](https://doc.asqatasun.org/v5/en/Developer/Documentation/Deploy/).

La commande `rsync` risque de ne pas marcher, je vous recommande donc de remplacer cette dernière par:

```
- scp -r "$CI_PROJECT_DIR" "$DEPLOY_USER@$DEPLOY_HOST:$DEPLOY_PATH"
```

## Projet Node.js, ...

Le Github Student Pack offre des crédits pour des serveurs gratuits, notamment AWS, Azure, DigitalOcean... Regardez le détail des offres sur chaque.